Proyectos de programación hechos mientras aprendia el lenguaje JAVA

Programas usados:
	
	-Eclipse

Proyectos:
	
	-Juegos: Tres en raya, Hundir la flota y busqueda del tesoro.
	
	El objetivo es aprender el uso de clases y tipos.
	

	-TiendaJuegos: Base de datos basado en una tienda de juegos
	con secciones de Hardware, Juegos y socios incluyendo opciones
	de visualizar y modificar.

	El objetivo es la creación y uso de objetos.