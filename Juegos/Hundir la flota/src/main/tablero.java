package main;

public class tablero {
	private int filas;
	private int columnas;
	private char[][] tablero;


	public tablero(int filas,int columnas){
		char [][]tablero=new char [filas][columnas];
		this.filas=filas;
		this.columnas=columnas;
		this.tablero=tablero;
		for (int i=0;i<tablero.length;i++){
			for(int j=0;j<tablero.length;j++){
				tablero[i][j]=' ';
			}
	}
	}
	public void MostrarTablero(){
		System.out.println("  1  2  3  4  5  6  7  8  9 ");
		for (int i=0;i<tablero.length;i++){
			System.out.print(i+1);
			for(int j=0;j<tablero.length;j++){
				System.out.print("["+tablero[i][j]+"]");
			}
			System.out.println();
		}
	}
	public boolean MarcarPosicion(int fila,int columna){
		boolean fallo=false;
		if(tablero[fila-1][columna-1]==' '){
			tablero[fila-1][columna-1]='O';
			return fallo;
	}else{
		fallo=true;
		return fallo;
	}
	}
	public boolean Disparo(int fila,int columna){
		boolean agua;
		if(tablero[fila-1][columna-1]=='O'){
			agua=false;
			tablero[fila-1][columna-1]='X';
		}else{
			agua=true;
		}
		return agua;
	}
	public boolean Comprobar(){
		boolean hundidos=false;
		int barcoshundidos=0;
		for (int i=0;i<tablero.length;i++){
			for(int j=0;j<tablero.length;j++){
				if(tablero[i][j]=='X'){
					barcoshundidos++;
				}
			}
	}
		if(barcoshundidos==3){
			hundidos=true;
		}
		return hundidos;
	}
	public boolean VerDisparos(int fila,int columna){
		boolean ocupado=false;
		if(tablero[fila-1][columna-1]=='X'){
			ocupado=true;
		}else{
		tablero[fila-1][columna-1]='X';
		}
		return ocupado;
	}
	}

