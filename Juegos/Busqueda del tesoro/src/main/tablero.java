package main;

public class tablero {
	private int filas;
	private int columnas;
	private int[][] tablero;


	public tablero(int filas,int columnas){
		int [][]tablero=new int [filas][columnas];
		this.filas=filas;
		this.columnas=columnas;
		this.tablero=tablero;
	}
	public void RellenarMapa(){
		for (int i=0;i<tablero.length;i++){
			for(int j=0;j<tablero.length;j++){
				tablero[i][j]=(int)(Math.random()*99)+1;
			}
	}
	}
	public void VisualizarMapaDatos(){
		for (int i=0;i<tablero.length;i++){
			for(int j=0;j<tablero.length;j++){
				System.out.print("["+tablero[i][j]+"]");
			}
			System.out.println();
		}
	}
	public void VisualizarMapaOro(){
		int suma;
		int media;
		int contador;
		String oro[][]=new String[9][9];
		for (int i=0;i<tablero.length;i++){
			for(int j=0;j<tablero.length;j++){
				suma=0;
				media=0;
				contador=0;
				if((i-1)>0){
					suma=suma+tablero[i-1][j];
					contador++;
				}
				if((i+1)<tablero.length){
					suma=suma+tablero[i+1][j];
					contador++;
				}
				if((j-1)>0){
					suma=suma+tablero[i][j-1];
					contador++;
				}
				if((j+1)<tablero.length){
					suma=suma+tablero[i][j+1];
					contador++;
				}
				media=suma/contador;
				if(media<tablero[i][j]){
					oro[i][j]="*";
				}else{
					oro[i][j]=" ";
				}
			}
		}
		System.out.println("  1  2  3  4  5  6  7  8  9 ");
		for (int i=0;i<tablero.length;i++){
			System.out.print(i+1);
			for(int j=0;j<tablero.length;j++){
				System.out.print("["+oro[i][j]+"]");
			}
			System.out.println();
		}
	}
}
