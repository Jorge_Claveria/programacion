
package main;
/**
 * Juegos
 * @author Jorge Claveria
 * @since Java 1.6
 * @version Java 1.8
 * 
 *
 */
import java.util.ArrayList;
public class Juegos {
private String consola;
private String nombre;
private String marca;
private int precio;
public Juegos(){
	this.nombre="";
	this.marca="";
	this.precio=0;
	this.consola="";
}
public Juegos(String tipo,String nombre,String marca,int precio){
	this.nombre=nombre;
	this.marca=marca;
	this.precio=precio;
	this.consola=tipo;
}
public String seleccionarConsola(){
	String consola=this.consola;
	return consola;
}
public String seleccionarNombre(){
	String nombre=this.nombre;
	return nombre;
}
public void mostrarJuegos(){
	String consola=this.consola;
	String nombre=this.nombre;
	String marca=this.marca;
	int precio=this.precio;
	System.out.println(consola+"-"+nombre+"-"+marca+"-"+precio);
}
public String modificarConsola(String consola){
	this.consola=consola;
	return consola;
}
public void modificarNombre(String nombre){
	this.nombre=nombre;
}
public void modificarMarca(String marca){
	this.marca=marca;
}
public void modificarPrecio(int precio){
	this.precio=precio;
}
}
