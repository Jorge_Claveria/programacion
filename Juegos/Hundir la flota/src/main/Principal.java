package main;
import java.util.Scanner;
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada=new Scanner(System.in);
		int fila;
		int columna;
		int contador=0;
		int contadorb=0;
		boolean repite=true;
		boolean fallorelleno=true;
		tablero jugador = new tablero(9,9);
		tablero ia = new tablero(9,9);
		tablero tusdisparos=new tablero(9,9);
		tablero disparosmaquina=new tablero(9,9);
		jugador.MostrarTablero();
		for(int i=0;i<3;i++){
			fallorelleno=true;
			while(fallorelleno){
		System.out.println("Introduzca un barco: \nFila:");
		fila=entrada.nextInt();
		System.out.println("Columna:");
		columna=entrada.nextInt();
		fallorelleno=jugador.MarcarPosicion(fila, columna);
		disparosmaquina.MarcarPosicion(fila, columna);
		if (fallorelleno){
			System.out.println("Posici�n no v�lida");
		}
		}
		}
		jugador.MostrarTablero();
		fallorelleno=true;
		for(int i=0;i<3;i++){
			while(fallorelleno){
				fila=(int)(Math.random()*9)+1;
				columna=(int)(Math.random()*9)+1;
				fallorelleno=ia.MarcarPosicion(fila, columna);
			}
		}
		while(repite){
			System.out.println("Dispara: \nFila: ");
			fila=entrada.nextInt();
			System.out.println("Columna:");
			columna=entrada.nextInt();
			if(ia.Disparo(fila, columna)==true){
				if(tusdisparos.VerDisparos(fila, columna)){
					System.out.println("Disparo repetido");
				}else{
				tusdisparos.MostrarTablero();
				System.out.println("Agua");
				contador++;
				}
			}else{
				if(tusdisparos.VerDisparos(fila, columna)){
					System.out.println("Disparo repetido");
				}else{
				tusdisparos.MostrarTablero();
				System.out.println("Hundido");
				contador++;
			}
			}
			System.out.println("Llevas "+contador+" disparos");
			fila=(int)(Math.random()*9)+1;
			columna=(int)(Math.random()*9)+1;
			if(jugador.Disparo(fila, columna)==true){
				if(disparosmaquina.VerDisparos(fila, columna)){
					System.out.println("Disparo repetido");
				}else{
				disparosmaquina.MostrarTablero();
				System.out.println("La IA ha dado en el agua");
				contadorb++;
				}
			}else{
				if(disparosmaquina.VerDisparos(fila, columna)){
					System.out.println("Disparo repetido");
				}else{
				disparosmaquina.MostrarTablero();
				System.out.println("La IA ha hundido el barco ("+fila+","+columna+")");
				contadorb++;
			}
			}
			System.out.println("La IA lleva "+contadorb+" disparos");
			if(jugador.Comprobar()){
				repite=false;
				System.out.println("La IA ha ganado");
			}
			if(ia.Comprobar()){
				repite=false;
				System.out.println("Has ganado");
			}
		}
	}

		}
