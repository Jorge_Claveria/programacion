package main;

public class tablero {
private int filas;
private int columnas;
private char[][] tablero;


public tablero(int filas,int columnas){
	char [][]tablero=new char [filas][columnas];
	this.filas=filas;
	this.columnas=columnas;
	this.tablero=tablero;
	for (int i=0;i<tablero.length;i++){
		for(int j=0;j<tablero.length;j++){
			tablero[i][j]=' ';
		}
}
}
public void MostrarTablero(){
	System.out.println("  1  2  3 ");
	for (int i=0;i<tablero.length;i++){
		System.out.print(i+1);
		for(int j=0;j<tablero.length;j++){
			System.out.print("["+tablero[i][j]+"]");
		}
		System.out.println();
	}
}
public boolean MarcarPosicion(int jugador,int fila,int columna){
	boolean fallo=false;
	if(tablero[fila-1][columna-1]==' '){
		if(jugador==1){
		tablero[fila-1][columna-1]='X';}
		else{
			tablero[fila-1][columna-1]='O';
		}
		
		return fallo;
}else{
	
	fallo=true;
	return fallo;
}
}
public boolean Comprobar(){
	boolean seguir=true;
	if(tablero[0][0]=='X'){
		if(tablero[1][1]=='X'){
			if(tablero[2][2]=='X'){
				seguir=false;
			}
		}else if(tablero[0][1]=='X'){
			if(tablero[0][2]=='X'){
				seguir=false;
			}
		}else if(tablero[1][0]=='X'){
			if(tablero[2][0]=='X'){
				seguir=false;
			}
		}
	}else if(tablero[1][0]=='X'){
		if(tablero[1][1]=='X'){
			if(tablero[1][2]=='X'){
				seguir=false;
			}
		}
	}else if(tablero[2][0]=='X'){
		if(tablero[1][1]=='X'){
			if(tablero[0][2]=='X'){
				seguir=false;
			}
		}else if(tablero[2][1]=='X'){
			if(tablero[2][2]=='X'){
				seguir=false;
			}
		}
	}
	return seguir;
}
public boolean ComprobarIa(){
	boolean seguir=true;
	if(tablero[0][0]=='O'){
		if(tablero[1][1]=='O'){
			if(tablero[2][2]=='O'){
				seguir=false;
			}
		}else if(tablero[0][1]=='O'){
			if(tablero[0][2]=='O'){
				seguir=false;
			}
		}else if(tablero[1][0]=='O'){
			if(tablero[2][0]=='O'){
				seguir=false;
			}
		}
	}else if(tablero[1][0]=='O'){
		if(tablero[1][1]=='O'){
			if(tablero[1][2]=='O'){
				seguir=false;
			}
		}
	}else if(tablero[2][0]=='O'){
		if(tablero[1][1]=='O'){
			if(tablero[0][2]=='O'){
				seguir=false;
			}
		}else if(tablero[2][1]=='O'){
			if(tablero[2][2]=='O'){
				seguir=false;
			}
		}
	}
	return seguir;
}
public boolean ComprobarEmpate(){
	boolean empate=true;
	for(int i=0;i<3;i++){
		for(int j=0;j<3;j++){
			if(tablero[i][j]==' '){
				 empate=false;
			}
		}
	}
	return empate;
}
}
