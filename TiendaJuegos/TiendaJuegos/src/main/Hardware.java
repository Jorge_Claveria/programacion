package main;
/**
 * Hardware
 * @author Jorge Claveria
 * @since Java 1.6
 * @version Java 1.8
 * 
 *
 */
import java.util.ArrayList;
public class Hardware {
private String nombre;
private String marca;
private int precio;
private String tipo;
public Hardware(){
	this.nombre="";
	this.marca="";
	this.precio=0;
	this.tipo="";
}
public Hardware(String tipo,String nombre,String marca,int precio){
	this.nombre=nombre;
	this.marca=marca;
	this.precio=precio;
	this.tipo=tipo;
}
public void mostrarHardware(){
	String nombre=this.nombre;
	String marca=this.marca;
	int precio=this.precio;
	String tipo=this.tipo;
	System.out.println(tipo+"-"+nombre+"-"+marca+"-"+precio);
}
public String seleccionarTipo(){
	String tipo=this.tipo;
	return tipo;
}
public void modificarTipo(String tipo){
	this.tipo=tipo;
}
public void modificarNombre(String nombre){
	this.nombre=nombre;
}
public void modificarMarca(String marca){
	this.marca=marca;
}
public void modificarPrecio(int precio){
	this.precio=precio;
}
}
