package main;
/**
 * Socios
 * @author Jorge Claveria
 * @since Java 1.6
 * @version Java 1.8
 * 
 *
 */
import java.util.ArrayList;
public class Socios {
private String nombre;
private String apellidos;
private int edad;
private String dni;
private String direccion;
private ArrayList<Juegos> reservas;
public Socios(){
	this.nombre="";
	this.apellidos="";
	this.edad=0;
	this.dni="";
	this.direccion="";
	this.reservas=new ArrayList<Juegos>();
}
public Socios(String nombre,String apellidos,int edad,String dni,String direccion){
	this.nombre=nombre;
	this.apellidos=apellidos;
	this.edad=edad;
	this.dni=dni;
	this.direccion=direccion;
	this.reservas=new ArrayList<Juegos>();
}
public void aņadirReserva(Juegos reservado){
	reservas.add(reservado);
}
public String seleccionarDNI(){
	String dni=this.dni;
	return dni;
}
public void mostrarSocios(){
	String nombre=this.nombre;
	String apellidos=this.apellidos;
	int edad=this.edad;
	String dni=this.dni;
	String direccion=this.direccion;
	System.out.println(nombre+"-"+apellidos+"-"+edad+"-"+dni+"-"+direccion);
}
public void mostrarReservas(){
	for(int i=0;i<reservas.size();i++){
		reservas.get(i).mostrarJuegos();
	}
}
public void modificarNombre(String nombre){
	this.nombre=nombre;
}
public void modificarApellidos(String apellidos){
	this.apellidos=apellidos;
}
public void modificarEdad(int edad){
	this.edad=edad;
}
public void modificarDNI(String dni){
	this.dni=dni;
}
public void modificarDireccion(String direccion){
	this.direccion=direccion;
}
public int obtenerEdad(){
	int edad=this.edad;
	return edad;
}
}
